package raging.goblin.swingutils;

import java.awt.*;

import javax.swing.*;

import raging.goblin.swingutils.LookAndFeel.LAFProperties;

public class RunDoubleSlider {

    public static void main(String[] args) {
        AntiAliaser.antiAliasing();
        LAFProperties lafProperties = TestLAFProperties.getInstance();
        LookAndFeel.loadLaf(new String[]{}, lafProperties);
        TestFrame frame = new TestFrame(lafProperties) {
            @Override
            public JComponent createContent() {
                JPanel panel = new JPanel(new BorderLayout(10, 10));
                JLabel valueLabel = new JLabel("40.000");
                panel.add(valueLabel, BorderLayout.WEST);
                DoubleSlider doubleSlider = new DoubleSlider(40.0, 0.0, 100.0, 0.5, true);
                doubleSlider.addChangeListener(e -> valueLabel.setText(String.format("%.3f", doubleSlider.getDoubleValue())));
                panel.add(doubleSlider, BorderLayout.CENTER);
                return panel;
            }
        };
        frame.getFrame().setVisible(true);
    }
}
