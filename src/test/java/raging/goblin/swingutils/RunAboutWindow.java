package raging.goblin.swingutils;

import java.util.Optional;

import raging.goblin.swingutils.LookAndFeel.LAFProperties;

public class RunAboutWindow {

    public static void main(String[] args) {
        AntiAliaser.antiAliasing();
        LAFProperties lafProperties = TestLAFProperties.getInstance();
        LookAndFeel.loadLaf(new String[]{}, lafProperties);
        AboutWindow aboutWindow = new AboutWindow(null,
            "SwingUtils",
            Optional.of("/icons/home.png"),
            Optional.of("Version 2.0"),
            Optional.of("https://gitlab.com/raginggoblin/swingutils"),
            Optional.of("Some Swing utilities that I use in various projects.<br>Use &lt;br&gt; to create multiline text."),
            Optional.of("License: GNU GENERAL PUBLIC LICENSE, Version 3"));
        aboutWindow.setVisible(true);
    }
}
