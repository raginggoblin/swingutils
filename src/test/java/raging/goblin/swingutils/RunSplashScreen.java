package raging.goblin.swingutils;

import java.util.Locale;

import raging.goblin.swingutils.LookAndFeel.LAFProperties;

public class RunSplashScreen {

    public static void main(String[] args) {
        AntiAliaser.antiAliasing();
        LAFProperties lafProperties = TestLAFProperties.getInstance();
        LookAndFeel.loadLaf(new String[]{}, lafProperties);
        Locale.setDefault(new Locale("nl", "NL"));
        SplashScreen splashScreen = new SplashScreen("SwingUtils", "/icons/home.png", lafProperties);
        ScreenPositioner.centerOnScreen(splashScreen);
        splashScreen.setVisible(true);
    }
}
