package raging.goblin.swingutils;

import raging.goblin.swingutils.LookAndFeel.LAFProperties;

public class RunHelpBrowser {

    public static void main(String[] args) {
        AntiAliaser.antiAliasing();
        LAFProperties lafProperties = TestLAFProperties.getInstance();
        LookAndFeel.loadLaf(new String[]{}, lafProperties);
        HelpBrowser helpBrowser = new HelpBrowser("/help.html", "Help", "Error loading helpfile at /help.html", "Error", lafProperties);
        helpBrowser.setVisible(true);
    }
}
