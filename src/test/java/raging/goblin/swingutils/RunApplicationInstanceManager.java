package raging.goblin.swingutils;

import javax.swing.*;

public class RunApplicationInstanceManager {

    public static void main(String[] args) {
        final String applicationName = "ApplicationInstanceManager";
        JFrame frame = new JFrame(applicationName);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 150);
        if (ApplicationInstanceManager.lock(applicationName, frame)) {
            frame.setVisible(true);
        } else {
            frame.dispose();
        }
    }
}
