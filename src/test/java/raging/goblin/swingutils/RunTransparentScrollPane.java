package raging.goblin.swingutils;

import java.awt.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.stream.IntStream;

import javax.swing.*;

import raging.goblin.swingutils.LookAndFeel.LAFProperties;

public class RunTransparentScrollPane {

    public static void main(String[] args) {
        AntiAliaser.antiAliasing();
        LAFProperties lafProperties = TestLAFProperties.getInstance();
        LookAndFeel.loadLaf(new String[]{}, lafProperties);
        TestFrame frame = new TestFrame(lafProperties) {
            @Override
            public JComponent createContent() {
                return new TransparentScrollPane(makeList());
            }
        };
        frame.getFrame().setVisible(true);
    }

    private static Component makeList() {
        DefaultListModel<String> m = new DefaultListModel<>();
        IntStream.range(0, 50)
            .mapToObj(i -> String.format("%05d: %s", i, LocalDateTime.now(ZoneId.systemDefault())))
            .forEach(m::addElement);
        return new JList<>(m);
    }
}
