package raging.goblin.swingutils;

import javax.swing.*;

import raging.goblin.swingutils.LookAndFeel.LAFProperties;

public class RunStringSeparator {

    public static void main(String[] args) {
        AntiAliaser.antiAliasing();
        LAFProperties lafProperties = TestLAFProperties.getInstance();
        LookAndFeel.loadLaf(new String[]{}, lafProperties);
        TestFrame frame = new TestFrame(lafProperties) {
            @Override
            public JComponent createContent() {
                return new StringSeparator("Test");
            }
        };
        frame.getFrame().setVisible(true);
    }
}
