package raging.goblin.swingutils;

import java.awt.*;

import javax.swing.*;

import raging.goblin.swingutils.LookAndFeel.LAFProperties;

public class RunColorButton {

    public static void main(String[] args) {
        AntiAliaser.antiAliasing();
        LAFProperties lafProperties = TestLAFProperties.getInstance();
        LookAndFeel.loadLaf(new String[]{}, lafProperties);
        TestFrame frame = new TestFrame(lafProperties) {
            @Override
            public JComponent createContent() {
                return new ColorButton(Color.CYAN.getRGB(), lafProperties);
            }
        };
        frame.getFrame().setVisible(true);
    }
}
