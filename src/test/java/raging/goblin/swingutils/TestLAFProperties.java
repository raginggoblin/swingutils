package raging.goblin.swingutils;

import raging.goblin.swingutils.LookAndFeel.LAFProperties;

public final class TestLAFProperties implements LAFProperties {

    private static TestLAFProperties instance;

    public static TestLAFProperties getInstance() {
        if(instance == null) {
            instance = new TestLAFProperties();
        }
        return instance;
    }
}
