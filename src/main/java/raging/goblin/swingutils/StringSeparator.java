/*
 * Copyright 2016, SwingUtils <https://gitlab.com/raginggoblin/swingutils>
 *
 * This file is part of SwingUtils.
 *
 *  SwingUtils is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwingUtils is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SwingUtils. If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.swingutils;

import javax.swing.*;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

/**
 * This component consists of a label followed by a line (JSeparator).
 */
public class StringSeparator extends JPanel {

    public StringSeparator(String text) {
        setLayout(new FormLayout(
            new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("left:default"),
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),},
            new RowSpec[]{
                FormSpecs.RELATED_GAP_ROWSPEC,
                FormSpecs.DEFAULT_ROWSPEC,}));

        add(new JLabel(text), "2, 2");
        add(new JSeparator(), "4, 2");
    }
}
