/*
 * Copyright 2021, SwingUtils <https://gitlab.com/raginggoblin/swingutils>
 *
 * This file is part of SwingUtils.
 *
 *  SwingUtils is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwingUtils is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SwingUtils. If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.swingutils;

import static raging.goblin.swingutils.LookAndFeel.Themes.FLAT_LIGHT_LAF;

import java.awt.*;

import com.formdev.flatlaf.FlatDarculaLaf;
import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatIntelliJLaf;
import com.formdev.flatlaf.FlatLightLaf;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class LookAndFeel {

    public static boolean loadLaf(String[] args, LAFProperties lafProperties) {
        if (lafProperties.getLaf() == null || lafProperties.getLaf().isEmpty()) {
            initLafProperties(lafProperties);
        }

        Themes theme = Themes.valueOf(lafProperties.getLaf());
        if (args.length != 0) {
            try {
                theme = Themes.valueOf(args[0]);
            } catch (Exception e) {
                throw new RuntimeException(args[0] + " is not a valid value for a theme, use one of: FLAT_DARK_LAF, FLAT_DARCULA_LAF, FLAT_LIGHT_LAF, FLAT_INTELLIJ_LAF");
            }
        }

        log.debug("Setting theme to " + theme);
        switch (theme) {
            case FLAT_DARK_LAF:
                return FlatDarkLaf.install();
            case FLAT_DARCULA_LAF:
                return FlatDarculaLaf.install();
            case FLAT_LIGHT_LAF:
                return FlatLightLaf.install();
            case FLAT_INTELLIJ_LAF:
                return FlatIntelliJLaf.install();
        }

        return false;
    }

    private static void initLafProperties(final LAFProperties lafProperties) {
        lafProperties.setLaf(FLAT_LIGHT_LAF.name());
    }

    public enum Themes {FLAT_DARK_LAF, FLAT_DARCULA_LAF, FLAT_LIGHT_LAF, FLAT_INTELLIJ_LAF}

    public interface LAFProperties {

        default String getLaf() {
            return FLAT_LIGHT_LAF.name();
        }

        default void setLaf(String laf) {
            // We forget it
        }

        default boolean is4k() {
            final int width = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getWidth();
            return width > 3800;
        }

        default void set4k(boolean is4k) {
            // We forget it
        }
    }
}
