/*
 * Copyright 2016, SwingUtils <https://gitlab.com/raginggoblin/swingutils>
 *
 * This file is part of SwingUtils.
 *
 *  SwingUtils is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwingUtils is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SwingUtils. If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.swingutils;

import java.awt.*;

import javax.swing.*;

import lombok.Getter;
import raging.goblin.swingutils.LookAndFeel.LAFProperties;

public abstract class TestFrame {

    @Getter
    private JFrame frame;

    public TestFrame(LAFProperties lafProperties) {
        frame = createFrame(lafProperties);
        frame.getContentPane().add(createContent());
    }

    public abstract JComponent createContent();

    private JFrame createFrame(final LAFProperties lafProperties) {
        JFrame frame = new JFrame("Test Frame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout());
        final boolean is4k = lafProperties.is4k();
        frame.setSize(is4k? 600 : 300, is4k ? 600 : 300);
        ScreenPositioner.centerOnScreen(frame);
        return frame;
    }
}
