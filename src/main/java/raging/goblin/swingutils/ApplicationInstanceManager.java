/*
 * Copyright 2016, SwingUtils <https://gitlab.com/raginggoblin/swingutils>
 *
 * This file is part of SwingUtils.
 *
 *  SwingUtils is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwingUtils is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SwingUtils. If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.swingutils;

import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.swing.*;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ApplicationInstanceManager {

    public static boolean lock(String applicationName, JFrame parent) {
        try {
            Path applicationDir = Path.of(System.getProperty("user.home"), "." + applicationName.toLowerCase());
            Files.createDirectories(applicationDir);
            Path lockFile = applicationDir.resolve(applicationName.toLowerCase() + ".lock");
            RandomAccessFile randomFile = new RandomAccessFile(lockFile.toFile(), "rw");
            FileChannel channel = randomFile.getChannel();

            if (channel.tryLock() == null) {
                log.warn("Instance already running...");
                String message = String.format("An instance of %s is already running, this instance will close.", applicationName);
                String title = String.format("%s already started", applicationName);
                JOptionPane.showMessageDialog(parent, message, title, JOptionPane.ERROR_MESSAGE);
                return false;
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return true;
    }
}