/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 TERAI Atsuhiro (https://github.com/aterai/java-swing-tips)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package raging.goblin.swingutils;

import java.awt.*;
import java.util.Objects;

import javax.swing.*;
import javax.swing.plaf.basic.BasicScrollBarUI;

public class TransparentScrollPane extends JScrollPane {

    public TransparentScrollPane(final Component child) {
        super(child);
    }

    @Override
    public boolean isOptimizedDrawingEnabled() {
        return false;
    }

    @Override
    public void updateUI() {
        super.updateUI();
        EventQueue.invokeLater(() -> {
            getVerticalScrollBar().setUI(new TranslucentScrollBarUI());
            setComponentZOrder(getVerticalScrollBar(), 0);
            setComponentZOrder(getViewport(), 1);
            getVerticalScrollBar().setOpaque(false);
        });
        setLayout(new TranslucentScrollPaneLayout());
    }

    private class TranslucentScrollBarUI extends BasicScrollBarUI {

        private final Color DEFAULT_COLOR = new Color(150, 150, 150, 50);
        private final Color DRAGGING_COLOR = new Color(160, 160, 160, 50);
        private final Color ROLLOVER_COLOR = new Color(170, 170, 170, 50);

        @Override
        protected JButton createDecreaseButton(int orientation) {
            return new ZeroSizeButton();
        }

        @Override
        protected JButton createIncreaseButton(int orientation) {
            return new ZeroSizeButton();
        }

        @Override
        protected void paintTrack(Graphics g, JComponent c, Rectangle r) {
            // Do nothing
        }

        @Override
        protected void paintThumb(Graphics g, JComponent c, Rectangle r) {
            JScrollBar sb = (JScrollBar) c;
            Color color;
            if (!sb.isEnabled() || r.width > r.height) {
                return;
            } else if (isDragging) {
                color = DRAGGING_COLOR;
            } else if (isThumbRollover()) {
                color = ROLLOVER_COLOR;
            } else {
                color = DEFAULT_COLOR;
            }
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setPaint(color);
            g2.fillRect(r.x, r.y, r.width, r.height);
            g2.dispose();
        }
    }

    private class TranslucentScrollPaneLayout extends ScrollPaneLayout {

        @Override
        public void layoutContainer(Container parent) {
            if (parent instanceof JScrollPane) {
                JScrollPane scrollPane = (JScrollPane) parent;

                Rectangle availR = scrollPane.getBounds();
                availR.setLocation(0, 0); // availR.x = availR.y = 0;

                Insets insets = parent.getInsets();
                availR.x = insets.left;
                availR.y = insets.top;
                availR.width -= insets.left + insets.right;
                availR.height -= insets.top + insets.bottom;

                Rectangle vsbR = new Rectangle();
                vsbR.width = 12;
                vsbR.height = availR.height;
                vsbR.x = availR.x + availR.width - vsbR.width;
                vsbR.y = availR.y;

                if (Objects.nonNull(viewport)) {
                    viewport.setBounds(availR);
                }
                boolean canScroll = canScroll((JScrollPane) parent);
                if (viewport != null && viewport.getView() != null && viewport.getView().getPreferredSize() != null) {
                    if (viewport.getView() instanceof Scrollable) {
                        canScroll = !((Scrollable) viewport.getView()).getScrollableTracksViewportHeight();
                    }
                }
                if (Objects.nonNull(vsb)) {
                    vsb.setVisible(canScroll);
                    vsb.setBounds(vsbR);
                }
            }
        }

        private boolean canScroll(JScrollPane scrollPane) {
            if (vsbPolicy == VERTICAL_SCROLLBAR_ALWAYS) {
                return true;
            } else if (vsbPolicy == VERTICAL_SCROLLBAR_NEVER) {
                return false;
            }

            // vsbPolicy == VERTICAL_SCROLLBAR_AS_NEEDED
            Rectangle availR = scrollPane.getBounds();
            boolean viewTracksViewportHeight = false;
            boolean isEmpty = (availR.width < 0 || availR.height < 0);
            Component view = (viewport != null) ? viewport.getView() : null;
            if (!isEmpty && view instanceof Scrollable) {
                Scrollable sv = (Scrollable) view;
                viewTracksViewportHeight = sv.getScrollableTracksViewportHeight();
            }
            Dimension viewPrefSize =
                    (view != null) ? view.getPreferredSize()
                            : new Dimension(0, 0);
            Dimension extentSize =
                    (viewport != null) ? viewport.toViewCoordinates(availR.getSize())
                            : new Dimension(0, 0);
            return !viewTracksViewportHeight && (viewPrefSize.height > extentSize.height);
        }
    }

    private class ZeroSizeButton extends JButton {

        private final Dimension ZERO_SIZE = new Dimension();

        @Override
        public Dimension getPreferredSize() {
            return ZERO_SIZE;
        }
    }
}
