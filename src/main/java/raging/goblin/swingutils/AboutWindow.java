/*
 * Copyright 2016, SwingUtils <https://gitlab.com/raginggoblin/swingutils>
 *
 * This file is part of SwingUtils.
 *
 *  SwingUtils is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwingUtils is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SwingUtils. If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.swingutils;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import javax.swing.*;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class AboutWindow extends JDialog {

    public AboutWindow(Frame parent,
                       String title,
                       Optional<String> iconResource,
                       Optional<String> versionText,
                       Optional<String> projectUrl,
                       Optional<String> aboutText,
                       Optional<String> licenceText) {

        super(parent);
        setModal(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setTitle(title);
        getContentPane().setLayout(new BorderLayout(40, 0));

        getContentPane().add(new JPanel(), BorderLayout.EAST);
        getContentPane().add(new JPanel(), BorderLayout.WEST);

        JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 0));
        getContentPane().add(topPanel, BorderLayout.NORTH);
        iconResource.ifPresent(resource -> {
            JLabel iconLabel = new JLabel(Icon.getIcon(resource));
            topPanel.add(iconLabel);
        });
        topPanel.add(new JLabel(String.format("<html><h1>%s</h1></html>", title)));

        JPanel centerPanel = new JPanel(new BorderLayout(0, 10));
        JPanel versionAndUrlPanel = new JPanel();
        versionAndUrlPanel.setLayout((new BoxLayout(versionAndUrlPanel, BoxLayout.PAGE_AXIS)));
        versionText.ifPresent(version -> versionAndUrlPanel.add(new JLabel(String.format("<html><i>%s</i></html>", version))));
        projectUrl.ifPresent(url -> {
            JLabel urlLabel = new JLabel(String.format("<html><i>%s</i></html>", url));
            urlLabel.addMouseListener(new UrlMouseAdapter(url));
            versionAndUrlPanel.add(urlLabel);
        });
        centerPanel.add(versionAndUrlPanel, BorderLayout.NORTH);
        aboutText.ifPresent(about -> centerPanel.add(new JLabel(String.format("<html>%s</html>", about)), BorderLayout.CENTER));
        licenceText.ifPresent(licence -> centerPanel.add(new JLabel(licence), BorderLayout.SOUTH));
        getContentPane().add(centerPanel, BorderLayout.CENTER);

        JPanel bottomPanel = new JPanel(new BorderLayout(0, 20));
        bottomPanel.add(new JPanel(), BorderLayout.SOUTH);
        getContentPane().add(bottomPanel, BorderLayout.SOUTH);

        pack();
        ScreenPositioner.centerOnScreen(this);
    }

    @AllArgsConstructor
    private class UrlMouseAdapter extends MouseAdapter {

        private String url;

        @Override
        public void mouseClicked(MouseEvent e) {
            if (Desktop.isDesktopSupported()) {
                try {
                    URI uri = new URI(url);
                    Desktop.getDesktop().browse(uri);
                } catch (IOException | URISyntaxException ex) {
                    log.error("Unable to open url: " + url);
                }
            }
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        }

        @Override
        public void mouseExited(MouseEvent e) {
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }
}
