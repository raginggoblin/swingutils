/*
 * Copyright 2016, SwingUtils <https://gitlab.com/raginggoblin/swingutils>
 *
 * This file is part of SwingUtils.
 *
 *  SwingUtils is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwingUtils is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SwingUtils. If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.swingutils;

import java.awt.*;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.*;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import raging.goblin.swingutils.LookAndFeel.LAFProperties;


public class SplashScreen extends JFrame {

    private ResourceBundle labels;

    public SplashScreen(String applicationName, String iconResource, LAFProperties lafProperties) {
        labels = ResourceBundle.getBundle("SplashScreen", Locale.getDefault());

        setTitle(labels.getString("loading") + " " + applicationName);
        setResizable(false);
        setAlwaysOnTop(true);
        setUndecorated(true);
        setSize(lafProperties.is4k() ? 800 : 400, lafProperties.is4k() ? 500 : 250);
        getContentPane().setLayout(new FormLayout(new ColumnSpec[]{
                ColumnSpec.decode("center:default:grow"),},
                new RowSpec[]{
                        RowSpec.decode("default:grow"),
                        RowSpec.decode("default"),
                        RowSpec.decode("default:grow"),
                        RowSpec.decode("default"),
                        RowSpec.decode("default:grow"),}));

        ImageIcon logo = Icon.getIcon(iconResource);
        setIconImage(logo.getImage());
        JLabel iconLabel = new JLabel("     " + applicationName, logo, JLabel.LEFT);
        iconLabel.setFont(iconLabel.getFont().deriveFont(Font.BOLD));
        getContentPane().add(iconLabel, "1, 2");

        JLabel messageLabel = new JLabel(labels.getString("loading") + "...");
        messageLabel.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(messageLabel, "1, 3, center, center");
    }
}
