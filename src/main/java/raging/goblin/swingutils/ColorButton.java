/*
 * Copyright 2016, SwingUtils <https://gitlab.com/raginggoblin/swingutils>
 *
 * This file is part of SwingUtils.
 *
 *  SwingUtils is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwingUtils is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SwingUtils. If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.swingutils;

import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.swing.*;

import raging.goblin.swingutils.LookAndFeel.LAFProperties;

/**
 * A button displaying a certain color, by clicking on the user will be capable of changing the color.
 */
public class ColorButton extends JButton implements MouseListener {

    private int colorValue;
    private ColorChooser chooser;

    private int width = 100;
    private int height = 30;

    public ColorButton(int colorValue, LAFProperties lafProperties) {
        this.colorValue = colorValue;
        this.chooser = new ColorChooser(getColor(), lafProperties);
        setIcon(getColoredIcon());
        addMouseListener(this);
        addComponentListener(new ResizeListener());
    }

    public void setColorValue(int colorValue) {
        this.colorValue = colorValue;
        chooser.setColor(getColor());
        setIcon(getColoredIcon());
    }

    private void updateIcon() {
        setIcon(getColoredIcon());
    }

    public int getColorValue() {
        return colorValue;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // Nothing to do
    }

    @Override
    public void mousePressed(MouseEvent e) {
        requestFocusInWindow();
        chooser.setColor(getColor());
        chooser.setVisible(true);
        if (chooser.isOkButtonPressed()) {
            colorValue = chooser.getColor().getRGB();
            setIcon(getColoredIcon());
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // Nothing to do
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // Nothing to do
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // Nothing to do
    }

    private Color getColor() {
        return new Color(colorValue);
    }

    private ImageIcon getColoredIcon() {
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = image.createGraphics();
        g2.setColor(getColor());
        g2.fillRoundRect(14, 10, width - 28, height - 20, 3, 3);
        return new ImageIcon(image);
    }

    private class ResizeListener extends ComponentAdapter {
        public void componentResized(ComponentEvent e) {
            width = e.getComponent().getWidth();
            height = e.getComponent().getHeight();
            updateIcon();
        }
    }
}
