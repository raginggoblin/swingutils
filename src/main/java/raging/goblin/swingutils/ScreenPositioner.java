/*
 * Copyright 2016, SwingUtils <https://gitlab.com/raginggoblin/swingutils>
 * 
 * This file is part of SwingUtils.
 *
 *  SwingUtils is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwingUtils is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SwingUtils. If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.swingutils;

import java.awt.*;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ScreenPositioner {

   public static void centerOnScreen(Window window) {
      Toolkit toolkit = Toolkit.getDefaultToolkit();
      Dimension resolution = toolkit.getScreenSize();

      int x = (int) (resolution.getWidth() / 2 - window.getWidth() / 2);
      int y = (int) (resolution.getHeight() / 2 - window.getHeight() / 2);

      window.setLocation(x, y);
   }
}