/*
 * Copyright 2016, SwingUtils <https://gitlab.com/raginggoblin/swingutils>
 *
 * This file is part of SwingUtils.
 *
 *  SwingUtils is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwingUtils is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SwingUtils. If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.swingutils;

import javax.swing.*;

public class DoubleSlider extends JSlider {

    private static final double MULTIPLIER = 1000.0;

    public DoubleSlider(double value, double minimum, double maximum, double stepSize, boolean snapToTicks) {
        super(multiply(minimum), multiply(maximum), multiply(value));
        setMinorTickSpacing(multiply(stepSize));
        setSnapToTicks(snapToTicks);
    }

    public double getDoubleValue() {
        return divide(getValue());
    }

    public void setDoubleValue(double value) {
        setValue(multiply(value));
    }

    private static int multiply(double value) {
        return (int) Math.round(MULTIPLIER * value);
    }

    private static double divide(int value) {
        return value / MULTIPLIER;
    }

   static {
      UIManager.put("Slider.paintValue", false);
   }
}
