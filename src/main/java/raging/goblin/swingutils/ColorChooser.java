/*
 * Copyright 2016, SwingUtils <https://gitlab.com/raginggoblin/swingutils>
 *
 * This file is part of SwingUtils.
 *
 *  SwingUtils is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwingUtils is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SwingUtils. If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.swingutils;

import java.awt.*;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

import raging.goblin.swingutils.LookAndFeel.LAFProperties;

/**
 * A dialog displaying a color chooser to choose a color.
 */
public class ColorChooser extends JDialog implements ChangeListener {

    private boolean okButtonPressed = false;
    private Color color;
    private JColorChooser colorChooser;
    private ResourceBundle labels;

    public ColorChooser(Color color, LAFProperties lafProperties) {
        labels = ResourceBundle.getBundle("ColorChooser", Locale.getDefault());

        JPanel actionPanel = new JPanel();
        getContentPane().add(actionPanel, BorderLayout.SOUTH);
        final ColumnSpec[] columnSpecs = {
            ColumnSpec.decode("default:grow"),
            FormSpecs.RELATED_GAP_COLSPEC,
            ColumnSpec.decode("max(40dlu;default)"),
            FormSpecs.RELATED_GAP_COLSPEC,
            FormSpecs.DEFAULT_COLSPEC,
            FormSpecs.RELATED_GAP_COLSPEC,
            ColumnSpec.decode("max(40dlu;default)"),
            FormSpecs.RELATED_GAP_COLSPEC,
            ColumnSpec.decode("max(5dlu;default)")};
        final RowSpec[] rowSpecs = {
            FormSpecs.DEFAULT_ROWSPEC,
            FormSpecs.RELATED_GAP_ROWSPEC,
            FormSpecs.DEFAULT_ROWSPEC,
            FormSpecs.RELATED_GAP_ROWSPEC,
            RowSpec.decode("max(5dlu;default)"),};
        actionPanel.setLayout(new FormLayout(columnSpecs, rowSpecs));

        JButton btnCancel = new LabelButton(labels.getString("cancel"));
        btnCancel.addActionListener(e -> {
            okButtonPressed = false;
            setVisible(false);
        });
        actionPanel.add(btnCancel, "3, 3, 2, 1");

        JButton btnOk = new LabelButton(labels.getString("ok"));
        btnOk.addActionListener(e -> {
            okButtonPressed = true;
            setVisible(false);
        });
        actionPanel.add(btnOk, "7, 3, 2, 1");

        colorChooser = new JColorChooser(color);
        colorChooser.getSelectionModel().addChangeListener(this);
        getContentPane().add(colorChooser, BorderLayout.CENTER);

        setSize(lafProperties.is4k() ? 1200 : 600, lafProperties.is4k() ? 850 : 425);
        setModalityType(Dialog.DEFAULT_MODALITY_TYPE);
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        color = colorChooser.getColor();
    }

    public boolean isOkButtonPressed() {
        return okButtonPressed;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
        colorChooser.setColor(color);
    }
}
