/*
 * Copyright 2016, SwingUtils <https://gitlab.com/raginggoblin/swingutils>
 *
 * This file is part of SwingUtils.
 *
 *  SwingUtils is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwingUtils is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SwingUtils. If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.swingutils;

import java.awt.*;
import java.io.IOException;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;

import lombok.extern.log4j.Log4j2;
import raging.goblin.swingutils.LookAndFeel.LAFProperties;

@Log4j2
public class HelpBrowser extends JFrame {

    public HelpBrowser(String helpFile, String title, String loadErrorMessage, String loadErrorTitle, LAFProperties lafProperties) {
        super(title);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(lafProperties.is4k() ? 1200 : 600, lafProperties.is4k() ? 1200 : 600);
        initGui(helpFile, loadErrorMessage, loadErrorTitle);
        ScreenPositioner.centerOnScreen(this);
    }

    private void initGui(String helpFile, String loadErrorMessage, String loadErrorTitle) {
        try {
            JTextPane helpPane = initHelpPane(helpFile);
            JScrollPane scrollPane = new JScrollPane(helpPane);
            getContentPane().add(scrollPane);

            JButton homeButton = new IconButton("/icons/home.png");
            homeButton.addActionListener(a -> {
                helpPane.setCaretPosition(0);
                scrollPane.getVerticalScrollBar().setValue(0);
            });
            getContentPane().add(homeButton, BorderLayout.SOUTH);

        } catch (IOException | BadLocationException e) {
            JOptionPane.showMessageDialog(HelpBrowser.this, loadErrorMessage, loadErrorTitle, JOptionPane.ERROR_MESSAGE);
            log.error("Unable to load help text", e);
        }
    }

    private JTextPane initHelpPane(final String helpFile)
            throws IOException, BadLocationException {

        JTextPane helpPane = new JTextPane();
        helpPane.setContentType("text/html");
        EditorKit kit = helpPane.getEditorKit();
        Document document = kit.createDefaultDocument();
        kit.read(getClass().getResourceAsStream(helpFile), document, 0);
        helpPane.setDocument(document);
        helpPane.setEditable(false);
        helpPane.addHyperlinkListener(l -> {
            if (HyperlinkEvent.EventType.ACTIVATED == l.getEventType()) {
                String description = l.getDescription();
                if (description != null && description.startsWith("#")) {
                    helpPane.scrollToReference(description.substring(1));
                }
            }
        });
        return helpPane;
    }
}
