package fr.pud.client.view.jsuggestfield;

public abstract class SuggestMatcher {
   
   public abstract boolean matches(String object, String searchWord);

}