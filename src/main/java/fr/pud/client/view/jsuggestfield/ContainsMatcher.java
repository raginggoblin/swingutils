package fr.pud.client.view.jsuggestfield;

public class ContainsMatcher extends SuggestMatcher {
   
   @Override
   public boolean matches(String dataWord, String searchWord) {
      boolean toReturn = dataWord.toString().toLowerCase().contains(searchWord);
      return toReturn;
   }
}