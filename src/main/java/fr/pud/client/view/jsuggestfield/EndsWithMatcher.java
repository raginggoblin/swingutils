package fr.pud.client.view.jsuggestfield;

public class EndsWithMatcher extends SuggestMatcher {
   
   @Override
   public boolean matches(String dataWord, String searchWord) {
      return dataWord.toString().toLowerCase().endsWith(searchWord);
   }
}