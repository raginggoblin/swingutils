package fr.pud.client.view.jsuggestfield;

public class StartsWithMatcher extends SuggestMatcher {
   
   @Override
   public boolean matches(String dataWord, String searchWord) {
      return dataWord.toString().toLowerCase().startsWith(searchWord);
   }
}